#!/bin/bash -eu

# Verify we have docker functioning properly in this context
docker info

# Try to push. If this fails, we probably need to login
docker build -t registry.gitlab.com/bff/img-cleanup.rs:latest .
docker push registry.gitlab.com/bff/img-cleanup.rs:latest
