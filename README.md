img-cleanup
===========

> A rust command line to tool to scan jekyll markdown for unused images

## Installation

```sh
git clone git@gitlab.com:bff/img-cleanup.rs.git
cd img-cleanup.rs
cargo build --release
sudo cp target/release/img-cleanup /usr/local/bin/
```

## Usage

```sh
cd path/to/jekyll
img-cleanup \
  --md-dir=_posts/ \
  --image-dir=img/ \
  --image-url=https://myblog.com/img/
```

 * md-dir -- the path to a directory of _only_ markdown files you want scanned for image tags in markdown and html
 * image-dir -- the path to a directory of _only_ image files you want to be checked against your markdown
 * image-url -- the base url where all images are served from

Output like this:

```sh
Unused https://myblog.com/img/2014-05-14-panopticon-prison-cuba.jpg
Unused https://myblog.com/img/logo.png
Missing https://myblog.com/img/spotlight-on-green-damask.jpg
Unused https://myblog.com/img/2015/length-is-capacity.png
Unused https://myblog.com/img/header.jpg
Unused https://myblog.com/img/flattr-badge-large.png
```

## How It Works

 1. All files are discovered recursively inside the `image-dir` path passed on the command line
   * All files regardless of file extension are included in the list
   * Symlinks are ignored
   * Invalid UTF-8 characters in a file name may cause a file to be ignored
 2. All files inside `md-dir` path are parsed for images
   * All files are parsed as markdown regardless of file extension
   * If an `<img>` contains multiple `src` attributes, the last one is used
 3. Image usage is tracked using a hash map
   * Remote images are ignored silently
   * The first time an image is found, its marked as 'Used'
   * Any images not already in the set are added as 'Missing'
   * Other images are considererd 'Unused'
 4. The 'Missing' and 'Unused' images in the hashmap are reported on STDOUT

### Assumptions

The accuracy of this tool relies on a few assumptions:

 * All same-origin images are located in a certain directory
 * Only post templates should be checked (not page templates)
 * All images are referenced in HTML or markdown (not CSS or JavaScript, for instance)
 * All images are referenced in `<img>` tags in src attributes (style attributes, `<style>` tags, data attributes, srcsets, etc).

If these assumptions have important exceptions for you, you may to double check the results OR work within these assumptions using the recommendations section below.

### Recommendations

To increase the accuracy of this tool, consider:

 * Moving all image assets into a common directory (ex: /img), checking all pages using `img-cleanup` to catch any posts with broken images
 * Moving all post images into a common root directory (ex: /img/content instead of /img)
 * Not referencing any images in /img/content from page templates

## Contributing

Issues and pull requests welcome!
