struct HtmlImgParser {
    state: HtmlState,
    buf: String,
    src: Option<String>
}

enum HtmlState {
    Ignore,
    Tag,
    ImgTag,
    SrcAttribute,
    SrcVal
}

impl HtmlImgParser {
    fn new() -> HtmlImgParser {
        HtmlImgParser {
            state: HtmlState::Ignore,
            buf: String::new(),
            src: None
        }
    }

    fn parse_char(&mut self, c: char) -> Option<String> {
        match self.state {
            HtmlState::Ignore => self.ignore_parser(c),
            HtmlState::Tag => self.tag_parser(c),
            HtmlState::ImgTag => return self.img_tag_parser(c),
            HtmlState::SrcAttribute => self.src_attribute_parser(c),
            HtmlState::SrcVal => self.src_val_parser(c),
        };
        None
    }

    fn ignore_parser(&mut self, c: char) {
        match c {
            '<' => {
                self.state = HtmlState::Tag;
            },
            _ => {}
        }
    }

    fn tag_parser(&mut self, c: char) {
        match c {
            'I'|'i'|'m'|'M'|'g'|'G'  => self.buf.push(c),
            _ => {
                self.state = HtmlState::Ignore;
                return;
            }
        }

        if self.buf.len() == 3 {
            if &self.buf.to_lowercase() == "img" {
                self.state = HtmlState::ImgTag;
            } else {
                self.state = HtmlState::Ignore;
            }
            self.buf.clear();
        }
    }

    fn img_tag_parser(&mut self, c: char) -> Option<String> {
        match c {
            '>' => {
                self.state = HtmlState::Ignore;
                let src = self.src.clone();
                self.src = None;
                return src;
            },
            's'|'S' => {
                self.state = HtmlState::SrcAttribute;
                self.buf.push('s');
            }
            _ => {}
        };
        None
    }

    fn src_attribute_parser(&mut self, c: char) {
        match c {
            'r'|'R'|'c'|'C'|'=' => self.buf.push(c),
            '"' => {
                self.buf.push(c);
                if &self.buf.to_lowercase() == "src=\"" {
                    self.state = HtmlState::SrcVal;
                } else {
                    self.state = HtmlState::ImgTag;
                }
                self.buf.clear();
            }
            '>' => {
                self.state = HtmlState::Ignore;
                self.buf.clear();
            }
            _ => {
                self.state = HtmlState::ImgTag;
                self.buf.clear();
            }
        }
    }

    fn src_val_parser(&mut self, c: char) {
        match c {
            '"' => {
                self.src = Some(self.buf.clone());
                self.state = HtmlState::ImgTag;
                self.buf.clear();
            }
            _ => {
                self.buf.push(c);
            }
        }
    }
}

pub fn parse_image_urls<'a, S: AsRef<str>>(html: S) -> Vec<String> {
    let mut parser = HtmlImgParser::new();
    let mut image_urls = vec!();

    for char in html.as_ref().chars() {
        if let Some(image_url) = parser.parse_char(char) {
            image_urls.push(image_url);
        }
    }

    image_urls
}

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! parses {
        ($test_name:ident, $html:expr => $( $image_url:expr ) , * ) => {
            #[test]
            fn $test_name() {
                let mut image_urls: Vec<String> = Vec::new();
                $( image_urls.push($image_url.to_owned()); )*
                assert_eq!(parse_image_urls($html), image_urls);
            }
        };
        ($test_name:ident, $html:expr) => {
            #[test]
            fn $test_name() {
                let no_urls: Vec<String> = vec!();
                assert_eq!(parse_image_urls($html), no_urls);
            }
        };
    }

    mod simple_cases {
        use super::*;

        parses!(img_with_slash, r#"<img src="foo" />"# => "foo");
        parses!(img_no_slash, r#"<img src="foo" />"# => "foo");
        parses!(two_img_tags, r#"<img src="abc"><img src="def">"# => "abc", "def");
        parses!(end_of_html, r#"<html><head><title>fascinating post</title></head><body><h1>My Page is So Great</h1><img alt="best ever seen" src="https://flickr.com/"></body></html>"# => "https://flickr.com/");
    }

    mod nonmatches {
        use super::*;

        parses!(img_without_src, "<img>");
        parses!(empty_string, "");
        parses!(non_img_src, r#"<faketag src="dont-do-it" />"#);
    }

    mod edge_cases {
        use super::*;

        parses!(last_src, r#"<img src="foo" src="bar">"# => "bar");
    }

}
