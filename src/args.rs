use getopts::{self, Options};

use std::ffi::OsStr;

fn build_options() -> Options {
    let mut opts = Options::new();
    opts.reqopt("b", "image-url", "set base url for images", "URL");
    opts.reqopt("i", "image-dir", "set image directory to search through", "DIR");
    opts.reqopt("m", "md-dir", "set markdown directory to parse for images", "DIR");
    opts.optflag("h", "help", "print this help menu");
    opts
}

pub fn parse_args<C: IntoIterator>(args: C) -> getopts::Result
    where C::Item: AsRef<OsStr> {
    build_options().parse(args)
}

pub fn print_usage() {
    println!("{}", build_options().usage(""));
}
