extern crate getopts;
extern crate pulldown_cmark;
extern crate url;
extern crate walkdir;

mod args;
mod file_walker;
mod html;
mod markdown;

use walkdir::WalkDir;

use std::env;

use self::args::{parse_args, print_usage};
use self::file_walker::find_image_urls;
use self::file_walker::Situation::{Unused, Missing};
use self::markdown::update_image_counts_from;

fn main() {
    // Cli arguments
    let args: Vec<String> = env::args().collect();
    let matches = parse_args(&args[1..])
        .expect("Unable to parse input");

    if matches.opt_present("h") {
        print_usage();
        return;
    }

    let image_base_url = matches.opt_str("image-url").expect("You must provide an --image-url");
    let image_dir = matches.opt_str("image-dir").expect("You must provide an --image-dir");
    let md_dir = matches.opt_str("md-dir").expect("You must provide an --md-dir");

    let mut images = find_image_urls(&image_dir, &image_base_url).expect("Unable to walk directory");
    for entry in WalkDir::new(&md_dir).into_iter().filter_map(|e| e.ok()) {
        let path = entry.path();
        if path.is_file() {
            update_image_counts_from(path, &image_base_url, &mut images);
        }
    }

    for (image, situation) in images {
        match situation {
            Missing | Unused => println!("{:?} {}", situation, image),
            _ => {}
        };
    }
}
