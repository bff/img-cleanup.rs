use pulldown_cmark::{Parser, Tag, Event};
use url::Url;

use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

use html::parse_image_urls;
use file_walker::Situation::{self, Unused, Used, Missing};

fn normalize_url(raw_url: &str, base_url: &Url) -> Option<Url> {
    match Url::parse(raw_url) {
        Ok(url) => {
            // TODO surface the fact that there are external images somehow...
            if url.scheme().starts_with("http") && url.host_str() == base_url.host_str() {
                return Some(url);
            }
        }
        _ => {
            let image_url = base_url.join(raw_url).unwrap();
            return Some(image_url);
        }
    }
    None
}

fn update_url(image_url: &str, base_url: &Url, counts: &mut HashMap<Url, Situation>) {
    if let Some(url) = normalize_url(image_url, base_url) {
        match counts.get(&url) {
            Some(&Unused) => {counts.insert(url, Used);}
            None => {counts.insert(url, Missing);}
            _ => {}
        }
    }
}

pub fn update_image_counts_from(path: &Path, image_base_url: &str, image_counts: &mut HashMap<Url, Situation>) {
    let mut file = File::open(path).unwrap(); // TODO: consider alternatives to unwrap
    let mut markdown = String::new();
    file.read_to_string(&mut markdown).unwrap(); // TODO consider alternatives to unwrap
    let mut base_url = Url::parse(image_base_url).unwrap(); // TODO consider alternatives to unwrap
    base_url.set_path("/");

    for event in Parser::new(&markdown) {
        match event {
            Event::Html(html) | Event::InlineHtml(html) => {
                for src in parse_image_urls(html) {
                    update_url(&src, &base_url, image_counts);
                }
            }
            Event::Start(Tag::Image(src, _)) => {
                update_url(&src, &base_url, image_counts);
            }
            _ => {}
        }
    }
}
