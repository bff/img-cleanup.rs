use url::Url;
use walkdir::WalkDir;

use std::collections::HashMap;
use std::error::Error;
use std::path::Path;

#[derive(Debug)]
pub enum Situation {
    Missing,
    Used,
    Unused
}

pub fn find_image_urls<P: AsRef<Path>>(dir: P, image_base_url: &str) -> Result<HashMap<Url, Situation>, Box<Error>> {
    let base_url = Url::parse(image_base_url)?;
    let walker = WalkDir::new(dir.as_ref())
        .into_iter()
        .filter_map(|result| result.ok());

    let mut image_counts = HashMap::new();
    for entry in walker {
        let path = entry.path();
        if !path.is_file() { continue; }

        if let Ok(naked_path) = path.strip_prefix(dir.as_ref()) {
            if let Some(naked_str) = naked_path.to_str() {
                if let Ok(image_url) = base_url.join(naked_str) {
                    image_counts.insert(image_url, Situation::Unused);
                }
            }
        }

    }

    Ok(image_counts)
}
