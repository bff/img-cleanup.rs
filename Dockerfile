FROM buildpack-deps:yakkety

MAINTAINER Bryce Fisher-Fleig <bryce@fisher-fleig.me>

WORKDIR /app

# Install rust toolchain
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain nightly

ENV PATH="$PATH:/root/.cargo/bin"

# Insall general tools
RUN apt-get update && \
    apt-get install -y jq

# Install kcov dependencies
RUN apt install -y --fix-missing \
      cmake \
      libcurl4-openssl-dev \
      zlib1g-dev \
      libdw-dev \
      binutils-dev \
      libiberty-dev

# Compile kcov
RUN git clone https://github.com/brycefisher/kcov.git \
 && mkdir kcov/build && cd kcov/build && pwd \
 && cmake .. && make && make install \
 && rm -rf kcov \
 && kcov --version
